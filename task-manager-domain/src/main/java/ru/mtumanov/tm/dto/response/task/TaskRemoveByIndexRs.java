package ru.mtumanov.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Task;

@NoArgsConstructor
public final class TaskRemoveByIndexRs extends AbstractTaskRs {

    public TaskRemoveByIndexRs(@Nullable final Task task) {
        super(task);
    }

    public TaskRemoveByIndexRs(@NotNull final Throwable err) {
        super(err);
    }

}