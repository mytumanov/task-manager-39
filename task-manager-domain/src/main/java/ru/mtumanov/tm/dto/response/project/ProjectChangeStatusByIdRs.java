package ru.mtumanov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

@NoArgsConstructor
public final class ProjectChangeStatusByIdRs extends AbstractProjectRs {

    public ProjectChangeStatusByIdRs(@Nullable final Project project) {
        super(project);
    }

    public ProjectChangeStatusByIdRs(@NotNull final Throwable err) {
        super(err);
    }

}