package ru.mtumanov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.SortSpecification;
import org.mybatis.dynamic.sql.render.RenderingStrategies;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.ITaskService;
import ru.mtumanov.tm.comparator.CreatedComparator;
import ru.mtumanov.tm.comparator.NameComparator;
import ru.mtumanov.tm.comparator.StatusComparator;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.entity.EntityEmptyException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.NameEmptyException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;
import ru.mtumanov.tm.model.Task;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.mybatis.dynamic.sql.SqlBuilder.isEqualTo;
import static org.mybatis.dynamic.sql.SqlBuilder.select;
import static ru.mtumanov.tm.dynamicmodel.TaskDynamicSql.*;

public class TaskService implements ITaskService {

    @NotNull
    final IConnectionService connectionService;

    public TaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Nullable
    private SortSpecification getComporator(@NotNull final Comparator comparator) {
        if (comparator instanceof CreatedComparator)
            return created;
        else if (comparator instanceof NameComparator)
            return name;
        else if (comparator instanceof StatusComparator)
            return status;
        else
            return null;
    }

    @Override
    @NotNull
    public Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        if (name.isEmpty())
            throw new NameEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(userId, task);
        return task;
    }

    @Override
    @NotNull
    public Task updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        if (name.isEmpty())
            throw new NameEmptyException();
        @NotNull final Task task = findOneById(userId, id);
        task.setName(name);
        task.setDescription(description);
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
                taskRepository.update(task);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return task;
    }

    @Override
    @NotNull
    public Task changeTaskStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        @NotNull final Task task = findOneById(userId, id);
        task.setStatus(status);
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
                taskRepository.update(task);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return task;
    }

    @Override
    @NotNull
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws AbstractException {
        if (projectId.isEmpty())
            return Collections.emptyList();
        if (userId.isEmpty() || userId == null)
            throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllByProjectId(userId, projectId);
        }
    }

    @Override
    @NotNull
    public List<Task> findAll(@NotNull final String userId) throws AbstractException {
        if (userId.isEmpty() || userId == null)
            throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllUserId(userId);
        }
    }

    @Override
    @NotNull
    public Task add(@NotNull final String userId, @NotNull final Task model) throws AbstractException {
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
                model.setUserId(userId);
                taskRepository.add(model);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return model;
    }

    @Override
    @NotNull
    public List<Task> findAll(@NotNull final String userId, @Nullable final Comparator<Task> comparator) throws AbstractException {
        if (userId.isEmpty() || userId == null)
            throw new UserIdEmptyException();
        if (comparator == null)
            return findAll(userId);
        if (getComporator(comparator) == null)
            return findAll(userId);
        @NotNull final List<Task> tasks;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            @NotNull final SelectStatementProvider selectStatement = select(id, created, name, description, tasktUserId, status, projectId)
                    .from(taskTable)
                    .where(tasktUserId, isEqualTo(userId))
                    .orderBy(getComporator(comparator))
                    .build()
                    .render(RenderingStrategies.MYBATIS3);
            tasks = repository.findAllComporator(selectStatement);
            if (tasks == null)
                return Collections.emptyList();
        }
        return tasks;
    }

    @Override
    @NotNull
    public Task findOneById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        if (userId.isEmpty() || userId == null)
            throw new UserIdEmptyException();
        if (id.isEmpty() || id == null)
            throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findOneById(userId, id);
        }
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Task model) throws AbstractException {
        if (userId.isEmpty() || userId == null)
            throw new UserIdEmptyException();
        if (model == null)
            throw new EntityEmptyException(Task.class.getName());
        removeById(userId, model.getId());
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        if (userId.isEmpty() || userId == null)
            throw new UserIdEmptyException();
        if (id.isEmpty() || id == null)
            throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
                taskRepository.removeById(userId, id);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Override
    public void clear(@NotNull final String userId) throws AbstractException {
        if (userId.isEmpty() || userId == null)
            throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
                taskRepository.clear(userId);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Override
    public int getSize(@NotNull final String userId) throws AbstractException {
        if (userId.isEmpty() || userId == null)
            throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.getSize(userId);
        }
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        if (userId.isEmpty() || userId == null)
            throw new UserIdEmptyException();
        if (id.isEmpty() || id == null)
            throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.existById(userId, id);
        }
    }

    @Override
    @NotNull
    public List<Task> findAll() throws SQLException {
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAll();
        }
    }

    @Override
    @NotNull
    public Collection<Task> set(@NotNull final Collection<Task> models) throws AbstractException {
        if (models == null)
            throw new EntityEmptyException(Task.class.getName());
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            try {
                @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
                taskRepository.clearAll();
                taskRepository.addAll(models);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        }
        return models;
    }

}
